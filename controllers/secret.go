/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	statehubv1alpha1 "gitlab.com/statehub/state-controller/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8s "k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *StateReconciler) getSecretForState(ctx context.Context, state *statehubv1alpha1.State) (*corev1.Secret, error) {
	foundSecret := &corev1.Secret{}
	err := r.Get(ctx, k8s.NamespacedName{
		Name:      state.Name,
		Namespace: state.Namespace,
	}, foundSecret)

	return foundSecret, err
}

func (r *StateReconciler) createSecretForState(ctx context.Context, state *statehubv1alpha1.State) (*corev1.Secret, error) {
	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      state.Name,
			Namespace: state.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(&state.ObjectMeta, statehubv1alpha1.SchemeBuilder.GroupVersion.WithKind("State")),
			},
		},
		StringData: map[string]string{},
	}

	ctrl.SetControllerReference(state, secret, r.Scheme)
	err := r.Create(ctx, secret)
	if err != nil {
		return nil, err
	}
	return secret, nil
}

func (r *StateReconciler) updateSecretForState(ctx context.Context, secret *corev1.Secret, params map[string]string) error {
	secret.StringData = params
	return r.Update(ctx, secret)
}
