/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	statehub "gitlab.com/statehub/openapi-go"
	statehubv1alpha1 "gitlab.com/statehub/state-controller/api/v1alpha1"
	cloudTypes "gitlab.com/statehub/statehub-cloud/pkg/types"
)

func (r *StateReconciler) updateEndpointsStatus(ctx context.Context, intersectingLocations []statehubv1alpha1.Location, foundState *statehubv1alpha1.State, remoteState statehub.State) []error {
	log := r.Log.WithValues("endpoint", foundState.Name)
	// We should not fail because of a spesific endpoint.
	// We need to show user about errors.
	errors := []error{}

	for _, intersectingLocation := range intersectingLocations {
		if !endpointInStatus(intersectingLocation, foundState.Status.Endpoints) {
			endpoint, err := r.getEndpoint(remoteState, intersectingLocation)
			if err != nil {
				errors = append(errors, err)
				continue
			}
			// If no endpoint exist create
			if endpoint == nil {
				// TODO statehub api - put location Service Principal or it will never be ready
				endpoint, err = r.createEndpoint(ctx, remoteState, intersectingLocation)
				if err != nil {
					errors = append(errors, err)
					continue
				}
			}
			log.Info(fmt.Sprintf("got endpoint: %+v", endpoint))
			if !endpoint.Ready {
				var err error
				switch intersectingLocation.Provider {
				case string(cloudTypes.Aws):
					approveRequest := r.StatehubClient.StateLocationsApi.StateLocationsControllerApproveAwsPle(ctx, foundState.Name, intersectingLocation.Region, endpoint.ID)
					_, err = approveRequest.Execute()
				case string(cloudTypes.Azure):
					approveRequest := r.StatehubClient.StateLocationsApi.StateLocationsControllerApproveAzurePle(ctx, foundState.Name, intersectingLocation.Region, endpoint.ID)
					_, err = approveRequest.Execute()
				}
				if err != nil {
					log.Info(fmt.Sprintf("could not request approve for endpoint %v", endpoint.Location))
					errors = append(errors, err)
				} else {
					log.Info(fmt.Sprintf("endpoint %v not ready", endpoint.Location))
					errors = append(errors, fmt.Errorf("endpoint for subnet %s is not ready", endpoint.Location.Subnet))
				}
				continue
			}
			log.Info(fmt.Sprintf("endpoint %+v ready", endpoint.Location))
			foundState.Status.Endpoints = append(foundState.Status.Endpoints, *endpoint)
			err = r.Status().Update(ctx, foundState)
			if err != nil {
				log.Error(err, "Failed to update State", "State.Namespace", foundState.Namespace, "State.Name", foundState.Name)
				errors = append(errors, err)
				continue
			}
			log.Info(fmt.Sprintf("updated state(%v) endpoint for location: %+v", foundState, endpoint.Location))
		}
	}

	return errors
}

func endpointInStatus(location statehubv1alpha1.Location, statusEndpoints []statehubv1alpha1.Endpoint) bool {
	for _, endpoint := range statusEndpoints {
		if endpoint.Location.Provider == location.Provider &&
			endpoint.Location.ProviderScope == location.ProviderScope &&
			endpoint.Location.Region == location.Region &&
			endpoint.Location.Subnet == location.Subnet &&
			endpoint.Location.Vnet == location.Vnet {
			return true
		}
	}
	return false
}

func (r *StateReconciler) createEndpoint(ctx context.Context, state statehub.State, location statehubv1alpha1.Location) (*statehubv1alpha1.Endpoint, error) {
	if location.Provider == string(cloudTypes.Aws) {
		whitelistRequest := r.StatehubClient.StateLocationsApi.StateLocationsControllerAddAwsWhitelistedPrincipal(ctx, state.Name, location.Region)
		_, _, err := whitelistRequest.Execute()
		if err != nil {
			return nil, err
		}
	}

	endpointID, err := r.CloudInstance.CreateEndpoint(&cloudTypes.CreateEndpointRequest{
		StateID:       state.Id,
		StateName:     state.Name,
		DryRun:        false, //TODO: Support dry run
		Region:        location.Region,
		Subnet:        location.Subnet,
		ServiceURI:    location.PrivateLinkService,
		Vnet:          location.Vnet,
		ProviderScope: location.ProviderScope,
		ResourceGroup: location.ResourceGroup,
	})

	if err != nil {
		return nil, err
	}

	log := r.Log.WithValues("endpoint for region", location.Region)
	log.Info("created endpoint", "ID", endpointID)

	endpoint := statehubv1alpha1.Endpoint{
		Ready:    false,
		Location: location,
		Service:  location.PrivateLinkService,
		ID:       endpointID,
	}

	return &endpoint, err
}
