/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	storagev1 "k8s.io/api/storage/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"

	k8s "k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	statehub "gitlab.com/statehub/openapi-go"
	statehubv1alpha1 "gitlab.com/statehub/state-controller/api/v1alpha1"
	cloudProvider "gitlab.com/statehub/statehub-cloud/pkg/cloud"
	statehubTypes "gitlab.com/statehub/statehub-cloud/pkg/types"
)

const stateFinalizer = "state.statehub.io/finalizer"

// StateReconciler reconciles a State object
type StateReconciler struct {
	client.Client
	Log                      logr.Logger
	Scheme                   *runtime.Scheme
	StatehubClient           *statehub.APIClient
	CloudInstance            cloudProvider.Cloud
	DefaultStorageClassState string
}

//+kubebuilder:rbac:groups=state.statehub.io,resources=states,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=state.statehub.io,resources=states/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=state.statehub.io,resources=states/finalizers,verbs=update
//+kubebuilder:rbac:groups=storage.k8s.io,resources=storageclasses,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=nodes,verbs=get;list

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the State object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.7.2/pkg/reconcile
func (r *StateReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("state", req.NamespacedName)

	foundState := &statehubv1alpha1.State{}
	err := r.Get(ctx, req.NamespacedName, foundState)
	if err != nil {
		// TODO: If not found it means it should be deleted. (Not handeling delete)
		if errors.IsNotFound(err) {
			log.Info("State resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get State resource")
		return ctrl.Result{}, err
	}
	log.Info(fmt.Sprintf("found state: %v.%v", foundState.Name, foundState.Namespace))
	// Found state object matching the remote state
	// TODO: after spec update

	// Check if the State instance is marked to be deleted, which is
	// indicated by the deletion timestamp being set.
	if !foundState.GetDeletionTimestamp().IsZero() {
		if controllerutil.ContainsFinalizer(foundState, stateFinalizer) {
			// Run finalization logic for stateFinalizer. If the
			// finalization logic fails, don't remove the finalizer so
			// that  we can retry during the next reconciliation.
			if err := r.finalizeState(ctx, foundState); err != nil {
				return ctrl.Result{}, err
			}
			log.Info("successfully ran finalizer ", "state:", foundState.Name)

			// Remove stateFinalizer. Once all finalizers have been
			// removed, the object will be deleted.
			controllerutil.RemoveFinalizer(foundState, stateFinalizer)
			err := r.Update(ctx, foundState)
			if err != nil {
				return ctrl.Result{}, err
			}
			log.Info("removed finalizer ", "state:", foundState.Name)
		}
		return ctrl.Result{}, nil
	}

	// Add finalizer for this CR
	if !controllerutil.ContainsFinalizer(foundState, stateFinalizer) {
		controllerutil.AddFinalizer(foundState, stateFinalizer)
		err = r.Update(ctx, foundState)
		if err != nil {
			return ctrl.Result{}, err
		}
		log.Info("added finalizer successfully", "state:", foundState.Name)
	}

	clusterLocations, err := r.getClusterLocations(ctx)
	if err != nil {
		return ctrl.Result{}, err
	}
	log.Info("got cluster locations", "locations:", clusterLocations)

	remoteState, _, err := r.StatehubClient.StatesApi.StatesControllerFindOneExecute(r.StatehubClient.StatesApi.StatesControllerFindOne(ctx, req.Name))
	if err != nil {
		return ctrl.Result{}, err
	}

	// This should be in the API. The API should only show intersecting locations when getting information about a state
	// It will be.
	intersectingLocations := getIntersactingLocations(remoteState, clusterLocations)
	log.Info("got intersecting locations", "locations:", intersectingLocations)

	if len(intersectingLocations) == 0 {
		return ctrl.Result{}, fmt.Errorf("no intersecting locations found for state")
	}

	updateErrors := r.updateEndpointsStatus(ctx, intersectingLocations, foundState, remoteState)

	foundStorageClass := &storagev1.StorageClass{}
	err = r.Get(ctx, k8s.NamespacedName{
		Name: remoteState.StorageClass.Name,
	}, foundStorageClass)
	if err != nil {
		if errors.IsNotFound(err) {
			if len(foundState.Status.Endpoints) == 0 {
				updateErrors = append(updateErrors, fmt.Errorf("failed to create new StorageClass %s - no endpoints found", remoteState.StorageClass.Name))
				errorsString := ""
				for _, v := range updateErrors {
					errorsString = fmt.Sprintln(errorsString, v.Error())
				}
				return ctrl.Result{}, fmt.Errorf(errorsString)
			}

			log.Info("endpoint list in status", "endpoints", foundState.Status.Endpoints)

			storageClass, _ := r.generateStorageClassForState(ctx, foundState, &remoteState.StorageClass)
			err = r.Create(ctx, storageClass)
			if err != nil {
				log.Error(err, "Failed to create new StorageClass", "StorageClass.Namespace", storageClass.Namespace, "StorageClass.Name", storageClass.Name)
				return ctrl.Result{}, err
			}
			log.Info(fmt.Sprintf("Created storageClass: %v", storageClass.Name))
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get StorageClass")
		return ctrl.Result{}, err
	}

	err = r.updateStorageClass(ctx, foundState, foundStorageClass)
	if err != nil {
		log.Error(err, "Failed to update StorageClass", "Name", foundStorageClass.Name)
		return ctrl.Result{}, err
	}
	log.Info("updated StorageClass allowed topologies")

	// return ctrl.Result{RequeueAfter: nextRun.Sub(r.Now())}, nil

	// We return all the errors that we got when creating the endpoints
	if len(updateErrors) > 0 {
		errorsString := ""
		for _, v := range updateErrors {
			errorsString = fmt.Sprintln(errorsString, v.Error())
		}
		return ctrl.Result{}, fmt.Errorf(errorsString)
	}

	return ctrl.Result{}, nil
}

func (r *StateReconciler) finalizeState(ctx context.Context, state *statehubv1alpha1.State) error {
	log := r.Log.WithValues("finalizer", state.Name)
	for _, endpoint := range state.Status.Endpoints {
		err := r.CloudInstance.DeleteEndpoint(ctx, state.Name, endpoint.ID, endpoint.Location.Vnet)
		if err != nil {
			return err
		}
		log.Info("deleted endpoint", "id:", endpoint.ID)
	}
	return nil
}

func getIntersactingLocations(state statehub.State, clusterLocations []statehubv1alpha1.Location) []statehubv1alpha1.Location {

	intersectingLocations := []statehubv1alpha1.Location{}

	for _, clusterLocation := range clusterLocations {
		switch clusterLocation.Provider {
		case string(statehubTypes.Aws):
			for _, stateLocation := range *state.Locations.Aws {
				if stateLocation.Status == "ok" { // TODO: get status ok from openapi instead of static string
					if stateLocation.Region == clusterLocation.Region {
						clusterLocation.PrivateLinkService = stateLocation.PrivateLinkService.Name
						intersectingLocations = append(intersectingLocations, clusterLocation)
					}
				}
			}
		case string(statehubTypes.Azure):
			for _, stateLocation := range *state.Locations.Azure {
				if stateLocation.Status == "ok" { // TODO: get status ok from openapi instead of static string
					if stateLocation.Region == clusterLocation.Region {
						// TODO: add endpoint check on azure
						clusterLocation.PrivateLinkService = stateLocation.PrivateLinkService.Id
						intersectingLocations = append(intersectingLocations, clusterLocation)
					}
				}
			}
		}
	}

	return intersectingLocations
}

// TODO: implementation not complete
func (r *StateReconciler) getClusterLocations(ctx context.Context) ([]statehubv1alpha1.Location, error) {

	locations := []statehubv1alpha1.Location{}

	nodeList := &corev1.NodeList{}
	listOpts := []client.ListOption{
		client.MatchingLabels{statehubTypes.StatehubStatusTopologyLabel: "enabled"},
	}
	err := r.List(ctx, nodeList, listOpts...)
	if err != nil {
		return nil, err
	}

	for _, node := range nodeList.Items {
		r.Log.Info(fmt.Sprintf("recived node labels %v", node.Labels))
		region := node.Labels[statehubTypes.StatehubRegionTopologyLabel]
		provider := node.Labels[statehubTypes.StatehubProviderTopologyLabel]
		providerScope := node.Labels[statehubTypes.StatehubProviderScopeTopologyLabel]
		subnet := node.Labels[statehubTypes.StatehubSubnetTopologyLabel]
		vnet := node.Labels[statehubTypes.StatehubVnetTopologyLabel]
		resourceGroup := node.Labels[statehubTypes.StatehubResourceGroupTopologyLabel]
		// TODO: append only unique
		locations = append(locations, statehubv1alpha1.Location{
			Region:        region,
			Provider:      provider,
			ProviderScope: providerScope,
			Subnet:        subnet,
			Vnet:          vnet,
			ResourceGroup: resourceGroup,
		})
	}

	//TODO: return only unique locations (Use hashset)
	return locations, nil
}

func (r *StateReconciler) getEndpoint(state statehub.State, location statehubv1alpha1.Location) (*statehubv1alpha1.Endpoint, error) {
	endpoint, err := r.CloudInstance.GetEndpoint(&statehubTypes.CreateEndpointRequest{
		StateID:       state.Id,
		StateName:     state.Name,
		DryRun:        false, //TODO: Support dry run
		Region:        location.Region,
		Subnet:        location.Subnet,
		ServiceURI:    location.PrivateLinkService,
		Vnet:          location.Vnet,
		ProviderScope: location.ProviderScope,
		ResourceGroup: location.ResourceGroup,
	})
	log := r.Log.WithValues("endpoint for region", location.Region)
	if err != nil {
		log.Error(err, "recived error when getting endpoint", "state", state.Name, "region", location.Region)
		return nil, err
	}

	if endpoint == nil {
		return nil, nil
	}

	log.Info("Recived endpoint", "ID", endpoint.ID, "Address", endpoint.Address, "Ready", endpoint.Ready)

	e := &statehubv1alpha1.Endpoint{
		Ready:    endpoint.Ready,
		Address:  endpoint.Address,
		Service:  location.PrivateLinkService,
		Location: location,
		ID:       endpoint.ID,
	}

	//TODO: Post whitelist to statehub API

	return e, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *StateReconciler) SetupWithManager(mgr ctrl.Manager) error {
	//TODO: change to States, and owned SC
	return ctrl.NewControllerManagedBy(mgr).
		For(&statehubv1alpha1.State{}).
		Owns(&storagev1.StorageClass{}).
		Complete(r)
}
