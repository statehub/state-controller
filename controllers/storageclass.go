/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"

	corev1 "k8s.io/api/core/v1"
	storagev1 "k8s.io/api/storage/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	statehub "gitlab.com/statehub/openapi-go"
	statehubv1alpha1 "gitlab.com/statehub/state-controller/api/v1alpha1"
	statehubTypes "gitlab.com/statehub/statehub-cloud/pkg/types"
)

var (
	defaultStorageClassAnnotation = "storageclass.kubernetes.io/is-default-class"
)

func (r *StateReconciler) generateStorageClassForState(ctx context.Context, s *statehubv1alpha1.State, remoteStorageClass *statehub.StorageClass) (*storagev1.StorageClass, error) {

	PersistentVolumeReclaimPolicy := corev1.PersistentVolumeReclaimDelete
	AllowVolumeExpansion := false
	VolumeBindingMode := storagev1.VolumeBindingMode(remoteStorageClass.VolumeBindingMode)
	StatehubProvisioner := "csi.statehub.io"
	storageClassParameters, err := r.getStorageClassParametersForState(ctx, s)
	if err != nil {
		return nil, err
	}

	sc := &storagev1.StorageClass{
		TypeMeta: metav1.TypeMeta{
			Kind:       "StorageClass",
			APIVersion: "storage.k8s.io/v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      remoteStorageClass.Name,
			Namespace: s.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(&s.ObjectMeta, statehubv1alpha1.SchemeBuilder.GroupVersion.WithKind("State")),
			},
		},
		Provisioner:          StatehubProvisioner,
		Parameters:           storageClassParameters,
		ReclaimPolicy:        &PersistentVolumeReclaimPolicy,
		MountOptions:         nil,
		AllowVolumeExpansion: &AllowVolumeExpansion,
		VolumeBindingMode:    &VolumeBindingMode,
		AllowedTopologies:    []corev1.TopologySelectorTerm{},
	}

	if r.DefaultStorageClassState == s.Name {
		sc.ObjectMeta.Annotations = map[string]string{
			defaultStorageClassAnnotation: "true",
		}
		foundStorageClasses := &storagev1.StorageClassList{}

		err = r.List(ctx, foundStorageClasses)
		if err != nil {
			return nil, err
		}
		for _, storageClass := range foundStorageClasses.Items {
			if storageClass.ObjectMeta.Annotations[defaultStorageClassAnnotation] == "true" {
				storageClass.ObjectMeta.Annotations[defaultStorageClassAnnotation] = "false"
				r.Update(ctx, &storageClass)
			}
		}
	}

	ctrl.SetControllerReference(s, sc, r.Scheme)
	return sc, nil
}

// TODO implement
//TODO should this move over to api/v1alpha1/state_types.go?
func (r *StateReconciler) getStorageClassParametersForState(ctx context.Context, s *statehubv1alpha1.State) (map[string]string, error) {

	// TODO: add filesystem
	// TODO: add statehub secret for the spesific organization

	params := map[string]string{}
	secretParams := map[string]string{}

	for _, endpoint := range s.Status.Endpoints {
		secretParams[endpoint.Location.Subnet] = endpoint.Address
	}

	params["state"] = s.Name

	endpointParamsJson, err := json.Marshal(secretParams)
	if err != nil {
		return nil, err
	}

	params["endpointsjson"] = string(endpointParamsJson)

	return params, nil
}

func (r *StateReconciler) updateStorageClass(ctx context.Context, s *statehubv1alpha1.State, storageClass *storagev1.StorageClass) error {
	newStorageClass := storageClass.DeepCopy()

	needReplace := false

	params, err := r.getStorageClassParametersForState(ctx, s)
	if err != nil {
		return err
	}

	if len(params) != 0 {
		if !reflect.DeepEqual(storageClass.Parameters, params) {
			newStorageClass.Parameters = params
			needReplace = true
		}
	}

	if !reflect.DeepEqual(storageClass.AllowedTopologies, []corev1.TopologySelectorTerm{getTopologySelectorTermsForState(s)}) {
		newStorageClass.AllowedTopologies = []corev1.TopologySelectorTerm{getTopologySelectorTermsForState(s)}
		needReplace = true
	}

	if needReplace {
		r.Log.Info(fmt.Sprintf("StorageClass %s needs to be updated, deleting it", storageClass.Name))
		err = r.Delete(context.TODO(), storageClass, client.Preconditions{
			UID: &storageClass.UID,
		})
		if err != nil {
			return err
		}
		r.Log.Info(fmt.Sprintf("Creating StorageClass %s", newStorageClass.Name))
		newStorageClass.ResourceVersion = ""
		return r.Create(ctx, newStorageClass)
	} else {
		return nil
	}

}

func getTopologySelectorTermsForState(s *statehubv1alpha1.State) corev1.TopologySelectorTerm {

	providersMap := map[string]bool{}
	regionsMap := map[string]bool{}
	scopesMap := map[string]bool{}
	subnetsMap := map[string]bool{}

	for _, endpoint := range s.Status.Endpoints {
		providersMap[endpoint.Location.Provider] = true
		regionsMap[endpoint.Location.Region] = true
		scopesMap[endpoint.Location.ProviderScope] = true
		subnetsMap[endpoint.Location.Subnet] = true
	}

	providers := []string{}
	regions := []string{}
	scopes := []string{}
	subnets := []string{}

	// TODO: better desing pattern
	for provider := range providersMap {
		providers = append(providers, provider)
	}
	for region := range regionsMap {
		regions = append(regions, region)
	}
	for scope := range scopesMap {
		scopes = append(scopes, scope)
	}
	for subnet := range subnetsMap {
		subnets = append(subnets, subnet)
	}

	term := corev1.TopologySelectorTerm{
		MatchLabelExpressions: []corev1.TopologySelectorLabelRequirement{
			{
				Key:    statehubTypes.StatehubProviderTopologyLabel,
				Values: providers,
			},
			{
				Key:    statehubTypes.StatehubRegionTopologyLabel,
				Values: regions,
			},
			{
				Key:    statehubTypes.StatehubProviderScopeTopologyLabel,
				Values: scopes,
			},
			{
				Key:    statehubTypes.StatehubSubnetTopologyLabel,
				Values: subnets,
			},
		},
	}

	return term
}
