/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	// "github.com/aws/aws-sdk-go/aws"
	// "github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	// "github.com/aws/aws-sdk-go/aws/session"
	// "github.com/aws/aws-sdk-go/service/ec2"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	statehub "gitlab.com/statehub/openapi-go"
	statehubv1alpha1 "gitlab.com/statehub/state-controller/api/v1alpha1"
	"gitlab.com/statehub/state-controller/controllers"
	cloudProvider "gitlab.com/statehub/statehub-cloud/pkg/cloud"
	//+kubebuilder:scaffold:imports
)

var (
	scheme                  = runtime.NewScheme()
	setupLog                = ctrl.Log.WithName("setup")
	statehubURL             = os.Getenv("STATEHUB_API")
	resyncPeriodInt64 int64 = 30
	clusterID               = os.Getenv("CLUSTER_NAME")
	statehubToken           = os.Getenv("STATEHUB_TOKEN")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(statehubv1alpha1.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	// Azure creds load
	// Check if file exists
	creds_dir_path := "/tmp/azurecreds"
	if _, err := os.Stat(creds_dir_path); !os.IsNotExist(err) {
		setupLog.Info("Loading azure creds")
		files, _ := os.ReadDir(creds_dir_path)
		for _, file := range files {
			// Load content from file
			content, _ := os.ReadFile(fmt.Sprint(creds_dir_path, "/", file.Name()))
			// Check if not empty
			if len(content) > 0 {
				// Set env variable
				setupLog.Info(fmt.Sprintf("AzureCreds - Adding env variable %s", file.Name()))
				os.Setenv(file.Name(), string(content))
			}
		}
	}

	// Aws creds load
	// Check if file exists
	creds_dir_path = "/tmp/awscreds"
	if _, err := os.Stat(creds_dir_path); !os.IsNotExist(err) {
		setupLog.Info("Loading aws creds")
		files, _ := os.ReadDir(creds_dir_path)
		for _, file := range files {
			// Load content from file
			content, _ := os.ReadFile(fmt.Sprint(creds_dir_path, "/", file.Name()))
			// Check if not empty
			if len(content) > 0 {
				// Set env variable
				setupLog.Info(fmt.Sprintf("AwsCreds - Adding env variable %s", file.Name()))
				os.Setenv(file.Name(), string(content))
			}
		}
	}

	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	var defaultStorageClassState string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	flag.StringVar(&defaultStorageClassState, "default-storage-class", "", "The name of the state to use for default storage class")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	// sess := session.Must(session.NewSession())
	// creds := stscreds.NewCredentials(sess, "myRoleArn")
	// ec2Client := ec2.New(sess, &aws.Config{Credentials: creds})

	// config := &statehub.Configuration{Host: "shimmy-develop.dev.statehub.io", Debug: true }
	config := statehub.NewConfiguration()
	uri, err := url.ParseRequestURI(statehubURL)
	if err != nil {
		setupLog.Error(err, "statehub uri api error")
		os.Exit(1)
	}
	config.Host = uri.Host
	config.Scheme = uri.Scheme
	config.HTTPClient = http.DefaultClient
	config.HTTPClient.Timeout = 8 * time.Second
	config.Debug = true
	config.AddDefaultHeader("Authorization", fmt.Sprintf("Bearer %s", statehubToken))
	statehubClient := statehub.NewAPIClient(config)

	_, _, err = statehubClient.StatesApi.StatesControllerFindManyExecute(statehubClient.StatesApi.StatesControllerFindMany(context.Background()))
	if err != nil {
		setupLog.Error(err, "statehub api error")
		os.Exit(1)
	}

	cloudInstance, err := cloudProvider.GetCloud()
	if err != nil {
		setupLog.Error(err, "error getting cloud")
		os.Exit(1)
	}

	resyncPeriod := time.Duration(resyncPeriodInt64) * time.Second
	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "954577fa.statehub.io",
		SyncPeriod:             &resyncPeriod,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.StateReconciler{
		Client:                   mgr.GetClient(),
		Log:                      ctrl.Log.WithName("controllers").WithName("State"),
		Scheme:                   mgr.GetScheme(),
		StatehubClient:           statehubClient,
		CloudInstance:            cloudInstance,
		DefaultStorageClassState: defaultStorageClassState,
		// EC2Client: ec2Client,

	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "State")
		os.Exit(1)
	}
	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
