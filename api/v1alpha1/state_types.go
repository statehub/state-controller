/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// StateSpec defines the desired state of State
type StateSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// StateStatus defines the observed state of State
type StateStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Slice of endpoints that the state is available from
	//+kubebuilder:validation:Optional
	Endpoints []Endpoint `json:"endpoints"`

	// The StorageClass related to this State
	//+kubebuilder:validation:Optional
	StorageClass string `json:"storage_class"`

	//+kubebuilder:validation:Optional
	FirstMissed string `json:"first_miss"`
}

// Location where the endpoint or node resides
type Location struct {
	Provider           string `json:"provider"`
	Region             string `json:"region"`
	ProviderScope      string `json:"provider_scope"`
	Subnet             string `json:"subnet"`
	PrivateLinkService string `json:"private_link_service"`
	Vnet               string `json:"vnet"`
	ResourceGroup      string `json:"resource_group"`
}

// TODO: add subnet - @ Location or @ Endpoint

// Endpoint represents the Private Link endpoint
type Endpoint struct {
	Location Location `json:"location"`
	Service  string   `json:"service"`
	Address  string   `json:"address"`
	Ready    bool     `json:"ready"`
	ID       string   `json:"id"`
}

// TODO: How will the CSI know which endpoint to use?
// TODO: get it from state via the API or get it from SC parameters

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// State is the Schema for the states API
type State struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   StateSpec   `json:"spec,omitempty"`
	Status StateStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// StateList contains a list of State
type StateList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []State `json:"items"`
}

func init() {
	SchemeBuilder.Register(&State{}, &StateList{})
}
